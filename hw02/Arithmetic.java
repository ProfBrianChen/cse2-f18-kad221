////// CSE 02
////// Kyra Dimaranan
///
public class Arithmetic{
  public static void main(String args[]){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt 
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    double totCostPant; //total cost of pants
    double totCostShirts; //total cost of shirts
    double totCostBelts; //total cost of belts
    double salesTaxPant; //sales tax on pants
    double salesTaxShirts; //sales tax on shirts
    double salesTaxBelts; //sales tax belts
    double totCost; //total cost of purchases pre-tax
    double totSalesTax; //total cost of sales tax
    double totPaid; //total paid including sales tax 
    
    totCostPant=(numPants*pantsPrice); //calculating total cost
    System.out.println("Total cost of pants is $"+totCostPant+"");
    salesTaxPant=(totCostPant*paSalesTax*100); //calculating sales tax on pants
    double salesTaxPantI = (int) (salesTaxPant); //converting sales tax to integer 
    salesTaxPantI /= 100.0; //dividing by 100 to get actual sales tax value
    System.out.println("Total sales tax on pants is $"+salesTaxPantI+"");
    
    totCostShirts=(numShirts*shirtPrice); //calculating total cost shirts
    System.out.println("Total cost of shirts is $"+totCostShirts+"");
    salesTaxShirts=(totCostShirts*paSalesTax*100); //calculating sales tax on shirts
    double salesTaxShirtsI = (int) (salesTaxShirts); //converting sales tax to integer 
    salesTaxShirtI /= 100.0; //dividing by 100 to get actual sales tax value
    System.out.println("Total sales tax on shirts is $"+salesTaxShirtsI+"");
    
    totCostBelts=(numBelts*beltCost); //calculating total cost belts
    System.out.println("Total cost of belts is $"+totCostBelts+"");
    salesTaxBelts=(totCostBelts*paSalesTax*100); //calculating total sales tax on belts
    double salesTaxBeltsI = (int) (salesTaxBelts); //converting sales tax to integer 
    salesTaxBeltsI /= 100.0; //dividing by 100 to get actual sales tax value
    System.out.println("Total sales tax on belts is $"+salesTaxBeltsI+"");
    
    totCost=totCostBelts+totCostShirts+totCostPant; //calculating total cost pre-tax
    System.out.println("Total cost of items pre-tax is $"+totCost+"");
    totSalesTax=salesTaxBeltsI+salesTaxShirtsI+salesTaxPantI; //calculating total sales tax 
    System.out.println("Total sales tax of items is $"+totSalesTax+"");
    totPaid=totCost+totSalesTax; //calculating total paid including tax
    System.out.println("Total paid including tax is $"+totPaid+"");
    
  }
}