///////// Kyra Dimaranan
//////// CSE 02
//////// This program will pick a random card from a deck using a random number generator
public class CardGenerator {
  public static void main(String[] args){
    int ranCard = (int) (Math.random()*51)+1; //Random number generator will pick card from 1 - 52
    String suit = ""; 
    String number = "";
    if (ranCard >= 0 && ranCard <= 13){
      suit = "diamonds";
    } //assigns numbers 0-13 to diamonds
    else if (ranCard >= 14 && ranCard <= 26){
      suit = "clubs";
    } //assigns numbers 14-26 to clubs
    else if(ranCard >= 27 && ranCard <= 39){
      suit = "hearts";
    } //assigns numbers 27-39 to hearts
    else if (ranCard >= 40 && ranCard <= 52){
      suit = "spades";
    } //assigns 40-52 to spades
  int remainder = ranCard % 13; //returns range to original range of 1-13
    switch( remainder ){ 
      case 0:
        number = "ace";
        break;
      case 1:
        number = "1";
        break;
      case 2:
        number = "2";
        break;
      case 3:
        number = "3";
        break;
      case 4:
        number = "4";
        break;
      case 5:
        number = "5";
        break;
      case 6:
        number = "6";
        break;
      case 7:
        number = "7";
        break;
      case 8: 
        number = "8";
        break;
      case 9:
        number = "9";
        break;
      case 10:
        number = "king";
        break;
      case 11:
        number = "queen";
        break;
      case 12:
        number = "jack";
        break;
    } 
  System.out.println("You picked "+ number + " of " + suit);
  }
}