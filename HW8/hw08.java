///////// Kyra Dimaranan
//////// CSE 002
/////// This program represents a deck of cards with an array and uses a method to shuffle the 'deck'

import java.util.Scanner;

public class hw08{
  
  public static void printArray(String[] list){ //general method for printing an array
    for (int i=0; i < list.length; i++){ //for each element of the array 'list', print out the element
      System.out.print(list[i] + " ");
    }
  }
  
  public static String[] shuffle(String[] list){
    int k=0; //initializes variable for the list elements
    for (int j=0; j < 75 ; j++){ //shuffles 75 times
      k = (int)(Math.random()*51+1); 
      String first = list[0]; //stores first lineup of array
      list[0] = list[k]; //replaces first array with randomized one
      list[k] = first; //replaces random with first 
    }
    return list;
  }
  
  public static String[] getHand(String[] list, int index, int numCards){
    String[] hand = new String[numCards]; //sets size of hand array to amount of cards there are
    for (int c=0; c < hand.length; c++){ //for each element of the hand array do the following:
      hand[c] = list[index - c]; //changes at last index
    }
    return hand;
  }

  public static void main(String[] args) { 
  Scanner scan = new Scanner(System.in); 
   //suits club, heart, spade or diamond 
  String[] suitNames={"C","H","S","D"};    //sets the symbols for the suits
  String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //numbers of each card
  String[] cards = new String[52]; //sets size of the array of cards
  String[] hand = new String[5]; //sets size for the array representing a hand
  int numCards = 5; 
  int again = 1; 
  int index = 51;
  for (int i=0; i<52; i++){ 
    cards[i]=rankNames[i%13]+suitNames[i/13]; 
    System.out.print(cards[i]+" "); 
  } 
  System.out.println();
  printArray(cards); 
  shuffle(cards);
  System.out.println("Shuffled"); //elements after the word 'shuffled' represent shuffled array
  printArray(cards); 
  while(again == 1){ 
     hand = getHand(cards,index,numCards); 
    System.out.println("Hand"); //elements after word 'hand' represent the hand picked
     printArray(hand);
     index = index - numCards;
     System.out.println("Enter a 1 if you want another hand drawn"); 
     again = scan.nextInt(); 
  }  
  } 

}