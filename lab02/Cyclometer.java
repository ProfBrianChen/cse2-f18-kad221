//////// CSE 02
/////// Kyra Dimaranan
////// Cyclometer meant to measure speed and distance. It will print the number of minutes each trip took then print the distance of each trip and the trips combined in miles
public class Cyclometer {
  public static void main (String[] args) {
    int secsTrip1=480; //Seconds first trip took
    int secsTrip2=3220; // Seconds second trip took
    int countsTrip1=1561; //Rotations of first trip
    int countsTrip2=9037; //Rotations of second trip
    double wheelDiameter=27.0; //Diameter of bike wheel
    double PI=3.14158; //value of pi
    int feetPerMile=5280; // feet in a mile
    int inchesPerFoot=12; // inches in a foot
    int secondsPerMinute=60; // seconds in a minute
    double distanceTrip1, distanceTrip2, totalDistance; //declaring int. variables
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+ "minutes and had "+
                      countsTrip1+" counts."); //printing how long Trip 1 took with how many rotations
    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+ "minutes and had "+
                      countsTrip2+" counts."); //prints how long trip 2 took with how many rotations
    distanceTrip1=(countsTrip1*wheelDiameter*PI)/inchesPerFoot/feetPerMile; //distance in miles
    distanceTrip2=(countsTrip2*wheelDiameter*PI)/inchesPerFoot/feetPerMile; //distance of trip 2 in miles
    totalDistance=distanceTrip1+distanceTrip2; // calculates total dist. in miles
    System.out.println("Trip 1 was "+distanceTrip1+"  miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  }
} //end of class