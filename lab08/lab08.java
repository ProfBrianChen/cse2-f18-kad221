////// Kyra Dimaranan
///// CSE 002
///// This program randomly places numbers into an array and then counts the occurance of each numbers

public class lab08{
  public static void main(String[] args){
    int[] randomNums; //declare array of random numbers
    final int arraySize1 = 100; 
    randomNums = new int[arraySize1]; //sets size of first array to 100 numbers 
    
    int c=0; //initialize counter for occurances
    int[] occurances; //declare second array 
    final int arraySize2 = 100; //size of second array
    occurances = new int[arraySize2]; //allocates size of second array to 100 numbers
    
    int i=0;
    for (i=0; i < randomNums.length; i++){
      randomNums[i] = (int)(Math.random()*98+1); //will generate numbers from 0-99
      c=randomNums[i];
      occurances[c]++;
    } //assigns a random number to each integer in the array
  
    for (int j=0; j <= arraySize2 ; j++){
      System.out.printf("%d occurs %d times\n", j, occurances[j]);
    }
    
 
  }
} 