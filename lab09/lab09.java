/////// Kyra Dimaranan
////// CSE 002
///// This program shows the effect of passing arrays as method arguments

public class lab09{
  
  public static int[] copy(int[] input){
    int[] output = new int[input.length]; 
    for (int i=0; i<input.length; i++){
      output[i] = input[i];
    }
    return output;
  } 
  
  public static void inverter(int[] input){
    for (int j=0; j<input.length/2; j++){
      int temp=input[j];
      input[j] = input[input.length-1-j];
      input[input.length-1-j]=temp;
    }
    //return temp;
  }
  
 public static int[] inverter2(int[] input){
   int[] output= copy(input); 
   inverter(output);
   return output;
 }
  
  public static void print(int[] array){
    for(int k=0; k<array.length; k++){
      System.out.print(array[k]);
    }
  }
  
  public static void main(String[] args){
    int[] array0 = {0, 1, 2, 3, 4, 5, 6, 7, 8}; 
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    System.out.println("");
    inverter2(array1);
    print(array1);
    System.out.println("");
    inverter(array2);
    int[] array3 = array2;
    print(array3);
  }
  
}