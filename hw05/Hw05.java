//////// This program generates five random numbers and determines whether or not they would classify as a poker hand
/////// Kyra Dimaranan
////// CSE 02
import java.util.Scanner;
public class Hw05{
  public static void main (String[] args){
    Scanner myScanner = new Scanner(System.in); 
    System.out.print("Input the number of hands to be generated: "); //asks user how many times program should generate a hand
    double numHand = myScanner.nextDouble(); //initialize number of hands
    int c=0; //initialize counter to increment while loop
    int ranCard; //initialize variable for first random card
    int number; //initialize variable for number
    int ace=0; //initialize counter for aces
    int twos=0; //initialize counter for 2s
    int threes=0; //initialize counter for 3s
    int fours=0; //initialize counter for 4s
    int fives=0; //initialize counter for 5s
    int sixes=0; //initialize counter for 6s
    int sevs=0; //initialize founter for 7s
    int eights=0;// initialize counter for 8s
    int nines=0; //initialize counter for 9s
    int tens=0; //initalize counter for 10s
    int js=0; //initialize counter for jacks
    int qs=0; //intialize counter for queens
    int ks=0; //initalize counter for kings
    int f=0; //initialize counter for inner while
    double fourKind=0; //initialize counter for four of a kind hand
    double threeKind=0; //initialize counter for three of a kind hand
    double twoPair=0; //initialize counter for two pair hand
    double onePair=0; //initialize counter for one pair hand
   
    while (c < numHand){ //runs loop for as long as how many hands there are
      f=0;
      while (f<5){  //runs operations for each card out of 5
        ranCard = (int) (Math.random()*51)+1; //random number generator generates number for first card of hand
        number= ranCard % 13; //reduces 52 cards to the 13 in a suit 
        switch (number) { //counts how many of each face value there are
          case 0:
            ace++;
            break;
          case 1:
            twos++;
            break;
          case 2:
            threes++;
            break;
          case 3:
            fours++;
            break;
          case 4:
            fives++;
            break;
          case 5:
            sixes++;
            break;
          case 6:
            sevs++;
            break;
          case 7:
            eights++;
            break;
          case 8:
            nines++;
            break;
          case 9:
            tens++;
            break;
          case 10:
            js++;
            break;
          case 11:
            qs++;
            break;
          case 12:
            ks++;
            break;   
        }
        f++; //increments counter for inner while loop and goes to the next card
      } 
      if ( ace==4 || twos==4 || threes==4 || fours== 4 || fives==4 || sixes==4 || sevs==4 || eights==4 || nines==4 || tens==4 || js==4 || qs==4 || ks==4){
        fourKind++; //if any of the counters = 4, there is a four of a kind
      }
      else if (ace==4 || twos==4 || threes==4 || fours== 4 || fives==4 || sixes==4 || sevs==4 || eights==4 || nines==4 || tens==4 || js==4 || qs==4 || ks==4){
        threeKind++; //if any of the counters = 3, there is a three of a kind
      }
      if (ace == 2){ //if any of the counters equal 2, there is a one pair
        onePair++;
      }
      if (twos ==2){
        onePair++;
      }
      if (threes == 2){
        onePair++;
      }
      if (fours == 2){
        onePair++;
      }
      if (fives == 2){
        onePair++;
      }
      if (sixes == 2){
        onePair++;
      }
      if (sevs == 2){
        onePair++;
      }
      if (eights == 2){
        onePair++;
      }
      if (nines == 2){
        onePair++;
      }
      if (tens == 2){
        onePair++;
      }
      if (js == 2){
        onePair++;
      }
      if (qs == 2){
        onePair++;
      }
      if (ks == 2){
        onePair++;
      }
      if (onePair == 2){
        twoPair++; //counts two one-pairs as a two pair
        onePair= onePair-2; //takes away the count for a onePair
      }
      c++; //increments counter for big while loop
    }
    double prob4Kind = fourKind/numHand; //calculates probability of four of a kind by dividing occurance by # hands
    double prob3Kind = threeKind/numHand; //calculates probability of a three of a kind
    double prob2Pair = twoPair/numHand; //calculates probability of two pairs
    double prob1Pair = onePair/numHand; //calulates probability of one pair 
      
    System.out.println("Number of loops: " + numHand);
    System.out.printf("Probability of Four of a Kind: %4.5f\n", prob4Kind);
    System.out.printf("Probability of Three of a Kind: %4.5f\n", prob3Kind);
    System.out.printf("Probability of Two Pair: %4.5f\n", prob2Pair); 
    System.out.printf("Probability of One Pair: %4.5f\n", prob1Pair);
  }
}