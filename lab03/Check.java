//////////// Kyra Dimaranan
/////////// CSE 02 Lab 03
///  Program uses scanner class to obtain original cost of the check and will determine
//// how much each person at dinner has to pay
import java.util.Scanner;
public class Check{
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the original cost of the check in the form xx.xx");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //converts percentage to decimal value
    System.out.print("Enter number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    double totalCost;
    double costPerPerson;
    int dollars; //whole dollar amount of cost
    int dimes, pennies; //storing digits to the right of decimal point for cost
    totalCost = checkCost*(1+tipPercent); //calculates total cost including tip
    costPerPerson = totalCost / numPeople; //calculating cost per person 
    dollars = (int) costPerPerson; //"whole" amount of cost w/o fraction
    dimes = (int) (costPerPerson*10) % 10; //get dime amount
    pennies = (int) (costPerPerson*100) % 10; //get pennies amount 
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
  }
}