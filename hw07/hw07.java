/////// Kyra Dimaranan
////// CSE 002
////// This program uses multiple methods to analyze user provided string
import java.util.Scanner;

public class hw07{
  
  public static String sampleText(String sample){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Enter a sample text: "); //prompts user for sample text
    String sampText = myScanner.nextLine();
    System.out.println("You entered: " + sampText); //shows user the text they input
    return sampText; //saves the text the user entered
  }
  
  public static char printMenu(){ //prints the menu options for the user
    System.out.println("MENU");
    System.out.println("c - Number of non-white characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    Scanner myScanner = new Scanner(System.in); //import scanner
    System.out.println("Choose an option: ");
    char userOp = myScanner.next().charAt(0); //reads option user inputs
    return userOp; //saves char user entered
  }
  
 public static int getNumOfNonWSCharacters(String text){
   int i=0; //integer for positioning of characters for user string
   int ct=0; //counter for how many non WS characters there are
   for (i=0; i < text.length(); i++){ //moves from character to character
     if (text.charAt(i) != ' ' && text.charAt(i) != '\t'){
       ct++; //incrememnts counter if character isn't a white space
     }
   }
   System.out.println("Number of non-white space charcters is: " + ct);
   return ct; 
 }
  
public static int getNumOfWords(String text){
  int i=0; //integer for positioning of characters of user string
  int ct=0; //initializes counter 
  for (i=0; i<text.length(); i++){ //moves from character to character in the string
    if (text.charAt(i) == ' '){ 
      ct++; //increment counter when a space is reached
    }
  }
  System.out.println("Number of words is: " + ct); //prints how many words there are
  return ct; //returns counter
}

public static int findText(String word, String text){
  int ct=0; //counter for how often the word is found
  ct= text.split(word, -1).length-1; // finds matched length of string user wants to search to user string entered
  System.out.println("'" + word + "' instances: " + ct); //prints how many times the word was found
  return ct;  //return counter
}

public static String replaceExclamation(String text){
  String replaced = ""; //initializes string for the new text
  int i=0; //integer for positioning of each character in the user input string
  for (i=0; i<text.length(); i++){ //moves from character to character
    if (text.charAt(i) == '!'){ //reads if there is an exclamation point
      replaced += '.'; //if there is an exclamation point replace it with a period
    }
    else {
      replaced += text.charAt(i); //if not keep the character as is
    }
  }
  return replaced; //returns the replaced text
}
  
public static String shortenSpace(String text){
  String replaced = ""; //initializes string for replaced text
  int i=0; //integer for positioning of each character in the user input string
  for (i=0; i < text.length(); i++){ //moves from character to character in the string
    if (text.charAt(i) == ' ' && text.charAt(i++) == ' '){ //reads if there are two consecutive spaces next to each other
      replaced += ' '; //replaces it with only one space
    }
    else{
      replaced += text.charAt(i); //if there aren't two consecutive spaces prints the original text
    }
  }
  return replaced; //returns the replaced text
}
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    String sample = " "; //initializes string variable for user input text
    sample = sampleText(sample); //calls method that asks user for their sample text and stores it
    int count = 0; //intializes counter for the various methods that need it
    int e=0; //variable for while loop to continuously call menu
    char option=' '; //variable for user option
    while (e==0){ //while loop that will continuously call menu 
     option = printMenu(); //calls method that outputs the menu
      switch (option){
        case 'c': //if 'c' is input by the user method that counts number of non-white space characters is called
          count = getNumOfNonWSCharacters(sample); //calls method
          break;
        case 'w': //if 'w' is input by the user method that counts the number of words is called
          count= getNumOfWords(sample); //calls method
          break; 
        case 'f': //if 'f' is input by the user method that counts how many time word is used is called
          System.out.println("Enter a word or phrase to be found: "); //prompts user for the word they want to find
          String word = myScanner.nextLine(); //variable for the word or phrase
          count = findText(word, sample); //calls method
          break;
        case 'r': //if 'r' is input by the user method that replaces exclamation points with periods is called
          String repText = replaceExclamation(sample); //variable for the replaced text + calls method
          System.out.println("New text: " + repText); //prints new text
          break;
        case 's': //if 's' is input by the user method that replaces double spaces with single spaces is called
          String newText = shortenSpace(sample); //variable for new text + calls method
          System.out.println("New text: " + newText); //prints new text
          break;
        case 'q': //if 'q' is input by the user the while loop is exited and will stop calling menu method
          e++; //increments while loop counter to exit while loop
          break;
        default: //if none of these choices are made it will call menu again
          System.out.println("Please enter valid option"); //tells user to input valid choice
          break;
      }
   } 
  }
  
}