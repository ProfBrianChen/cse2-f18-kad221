///////// Kyra Dimaranan
//////// CSE 02
/////// This program prints out integers between 1-10 and arranges them into a pyramid
import java.util.Scanner;
public class PatternA{
  public static void main (String[] args){
    Scanner myScanner = new Scanner( System.in ); //imports scanner
    boolean correct; //sets condition to check if user input an integer 
    int c=0; //initialize variable that increments while loop
    int userInt=0; //initalize variable user int
    while (c==0){
      System.out.print("Enter an integer between 1-10: "); //prompts user for integer btwn 1 and 10
      correct = myScanner.hasNextInt(); //checks input
      if (correct){
        userInt = myScanner.nextInt(); //if user input an integer it will check if its between 1 and 10
        if ((userInt < 1) || (userInt > 10)){ //checks if its between 1 and 10
          myScanner.next(); //if it isnt discard it
          System.out.println("Please put an integer between 1-10");
        }
        else{
          c++; //if it is exit while loop
        }
      }
      else{
        myScanner.next(); //if input isnt an integer discard it
        System.out.println("Please input an integer.");
      }
    }
    for(int numRows = 1; numRows < (userInt+2) ; numRows++){ //controls how many rows there are
      for (int i=1; i < numRows; i++){ //starting with i=1, keep printing i in a row as long as i<numRow its in
        System.out.print(i + " ");
      }
      System.out.println("");
    }
  }
}