/////// Kyra Dimaranan
/////// CSE 02
/////// This program will take the dimensions of a pyramid given by the user and return the volume inside the pyramid
import java.util.Scanner;
public class Pyramid{
  public static void main (String[] args){
    Scanner myScanner = new Scanner( System.in ); //import Scanner class
    System.out.print("The square side of the pyramid is (input length) : "); //prompt user for square length 
    double squareLength = myScanner.nextDouble(); //declare variable for square length
    System.out.print("The height of the pyramid is (input height) : "); //prompt user for height of pyramid
    double pyHeight = myScanner.nextDouble(); //declare variable for pyramid height
    double pyVolume; //declare variable for pyramid volume 
    pyVolume=((squareLength*squareLength*pyHeight)/3); //calculates volume of pyramid
    System.out.println("The volume inside the pyramid is:" + pyVolume); //prints out volume inside pyramid
  }
}