///////// Kyra Dimaranan
//////// CSE 02
/////// This program converts the quantity of rain dropped in a hurricane into cubic miles
import java.util.Scanner;
public class Convert{
  public static void main (String[] args){
    Scanner myScanner = new Scanner( System.in ); //importing scanner class
    System.out.print("Enter the affected area in acres:"); //prompt user to enter affected area in acres
    double acresLand = myScanner.nextDouble(); //declare variable for acres of land and moves to next line
    System.out.print("Enter the rainfall in the affected area in inches:"); //prompt user to enter inches of rain on avg
    double rainFall = myScanner.nextDouble(); //declare variable for rainfall in inches and moves to next line
    double cubicMiles; //declare variable for final answer of cubic miles of rain
    double galtoMile = 9.08169e-13; //conversion from gallons to cubic miles
    double inchAcre = 27154; //gallons of rain if it was one acre of land covered in one inch of rain 
    double galRain; //gallons of rain
    galRain=((rainFall*acresLand)*inchAcre); //converting from amount of rain over amount of acres to total gallons of rain
    cubicMiles=(galRain*galtoMile); //converting gallons of rain to cubic miles
    System.out.println(+cubicMiles + " cubic miles"); 
  }
}