/////////// Kyra Dimaranan
////////// CSE 002
///////// Asks user for an integer which will be the dimensions of a square in which the message x will be hidden
import java.util.Scanner; //import scanner
public class EncryptedX{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    int c=0; //initializes integer that increments while loop
    boolean correct; //used to check if 
    int userInt = 0;
    
    while (c==0){  //sets counter for while loop that will check in the user input
      System.out.print("Input integer betweenn 0-100: ");
      correct = myScanner.hasNextInt(); //checks if input is an integer
      if (correct){ //if it is an integer it will go to the next checks
        userInt = myScanner.nextInt();
        if (userInt < 0 || userInt > 100){
          myScanner.next(); //discards input
          System.out.println("Please input integer between 0-100."); //if it is an integer but isn't in range user needs to re-input
          }
        else{
          c++; //if it is an integer and is in range while loop is exited
        }
      }
      else{
        myScanner.next(); //if it isn't even an integer it gets discarded
        System.out.println("Please input an integer.");
      }
    }
    
    for (int numRows = 0; numRows < userInt; numRows++) { //controls how many rows there are
      for (int i = 0; i < userInt ; i++){ //controls how many inputs in a row
        if (numRows == i){ //if the position of an input is = # rows, there will be a space
          System.out.print(" ");
        }
        else if (i == userInt-(numRows+1)){ //if the position of an input is i-1, there will be a space
          System.out.print(" ");
        }
        else{
          System.out.print("*"); //every other input will be a star
        }
      }
      System.out.print("\n"); //prints to next line
    }
    
  }
}