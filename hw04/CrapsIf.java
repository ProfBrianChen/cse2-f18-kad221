///////// Kyra Dimaranan
//////// CSE 02
/////// This program either takes input from the user or generates two random numbers representing dice to state the outcome/slang used from 'rolling'
/// in the game craps. This program specifically only uses if statements.
import java.util.Scanner;
public class CrapsIf {
  public static void main (String[] args){
    Scanner myScanner = new Scanner( System.in ); //imports scanner
    System.out.print("Type 'random' if you want randomly cast dice. Type 'input' if you want to input numbers."); //prompts user if they want randomly generated numbers or inputs
    String want = myScanner.nextLine(); //declares string variable and moves to next line
    int dice1 = 0;
    int dice2 = 0;
    if (want.equals("random")){
      dice1 = (int)(Math.random()*4)+2; //random number generator will pick value for first dice
      dice2 = (int)(Math.random()*4)+2; //ramdom number generator will pick value for second dice 
    }
    else if (want.equals("input")){
      Scanner my2Scanner = new Scanner (System.in);
      System.out.print("Input first dice value");
      dice1 = my2Scanner.nextInt();
      System.out.print("Input second dice value");
      dice2 = my2Scanner.nextInt();
      if (dice1 <= 0 && dice1 >= 7){
        System.out.println("Number not in range"); 
      } //checks if first value is in range
      if (dice2 <= 0 && dice2 >= 7){
        System.out.println("Number not in range");
      } //checks if second value is in range
    } //if user wants to input numbers program asks them to input first and second values
    int dSum; //variable for the sum of dice 1 + dice 2
    dSum=(dice1+dice2); 
    if (dSum == 2){
      System.out.println("Snake Eyes");
    }
    else if (dSum == 3){
      System.out.println("Ace Deuce");
    }
    else if ((dSum == 4) && (dice1 == 2)){
      System.out.println("Hard Four");
    }
    else if (dSum == 4) {
      System.out.println("Easy Four");
    }
    else if (dSum == 5) {
      System.out.println("Fever Five");
    }
    else if ((dSum == 6) && (dice1 == 6)){
      System.out.println("Hard Six");
    }
    else if (dSum == 6){
      System.out.println("Easy Six");
    }
    else if (dSum == 7){
      System.out.println("Seven Out");
    }
    else if ((dSum == 8) && (dice1 == 4)){
      System.out.println("Hard Eight");
    }
    else if (dSum == 8){
      System.out.println("Easy Eight");
    }
    else if (dSum == 9){
      System.out.println("Nine");
    }
    else if ((dSum == 10) && (dice1 == 5)){
      System.out.println("Hard Ten");
    }
    else if (dSum == 10){
      System.out.println("Easy Ten");
    }
    else if (dSum == 11){
      System.out.println("Yo-leven");
    }
    else if (dSum == 12){
      System.out.println("Boxcars");
    }
  }
}
