/////////// Kyra Dimaranan
////////// CSE 02
///////// this program takes either input from the user or randomly generated numbers and will tell you the slang term
//////// from the game craps. This program specifically only uses switch statements.
import java.util.Scanner;
public class CrapsSwitch {
  public static void main (String[] args){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Type '1' if you want randomly cast dice. Type '2' if you want to input numbers.");
    int want = myScanner.nextInt();
    int dice1 = 0;
    int dice2 = 0; 
    switch (want){
      case 1:
        dice1 = (int)(Math.random()*4)+2;
        dice2 = (int)(Math.random()*4)+2;
        break;
      case 2:
         Scanner my2Scanner = new Scanner (System.in);
         System.out.print("Input first dice value");
         dice1 = my2Scanner.nextInt();
         System.out.print("Input second dice value");
         dice2 = my2Scanner.nextInt();
        break;
    }
    int dSum;
    dSum=(dice1+dice2); 
    switch (dSum){
      case 2: 
        System.out.println("Snake Eyes");
        break;
      case 3:
        System.out.println("Ace Deuce");
        break;
      case 4:
        switch (dice1){
          case 1:
            System.out.println("Easy Four");
            break;
          case 2:
            System.out.println("Hard Four");
            break;
          case 3:
            System.out.println("Easy Four");
            break;
        }
        break;
      case 5:
        System.out.println("Fever Five");
        break;
      case 6:
        switch (dice1){
          case 3:
            System.out.println("Hard Six");
            break;
          default:
            System.out.println("Easy Six");
        }
        break;
      case 7:
        System.out.println("Seven out");
        break;
      case 8:
        switch (dice1){
          case 4:
            System.out.println("Hard Eight");
            break;
          default:
            System.out.println("Easy Eight");
        }
        break;
      case 9:
        System.out.println("Nine");
        break;
      case 10: 
        switch (dice1){
          case 5:
            System.out.println("Hard Ten");
            break;
          default:
            System.out.println("Easy Ten");
        }
        break;
      case 11: 
        System.out.println("Yo-leven");
        break;
      case 12:
        System.out.println("Boxcars");
        break;
    }
  }
}