//////// Kyra Dimaranan
//////// CSE 002
//////// Second program for homework 9

import java.util.Scanner;

public class RemoveElements{
  
  public static int[] randomInput(){ //method for filling an array with random numbers
    int[] randArray = new int[10]; //declares random array and sets length to 10 inputs
    for (int j=0; j < randArray.length; j++){
      randArray[j] = (int)(Math.random()*8+1); //for each element of the array, fill with random number
    }
    return randArray;
  }
  
  public static int[] delete(int[] list, int pos){ 
    int[] dlist = new int[9]; //allocates new array one element shorter than original array
    int i=0; //variable for position of an element in an array
    for (i=0; i<dlist.length; i++){ //for each element in the smaller array, do the following:
      if (i < pos){ //if the position of the element is less than the position given by the user, then:
        dlist[i] = list[i]; //copy all elements up to but not including element in given position
      }
      else if(i == pos || i > pos){ //if the position of the element of an array is greater than or equal to the position stated:
       dlist[i] = list[i+1]; //shift all elements to the left 
      }
    }
    return dlist; 
  }
  
  public static int[] remove(int[] list, int target){
    int count=0; //initializes counter for how often the target score occurs
    for (int p=0; p < list.length; p++){ 
      if (list[p] == target){
        count++; //counts how many times the target is found in the original array
      }
    }
    int[] rlist = new int[list.length-count]; //sets new array to the length of the original minus the amount of times the target was found
    for (int k=0; k < list.length; k++){
      if (list[k] == target){  //if the contents of the original array match the target, remove it from the array 
        rlist = delete(list, k);
      }
    }
    return rlist;
  }
 
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
      int num[]=new int[10];
      int newArray1[];
      int newArray2[];
      int index,target;
        String answer="";
        do{
          System.out.print("Random input 10 ints [0-9]");
          num = randomInput();
          String out = "The original array is:";
          out += listArray(num);
          System.out.println(out);

          System.out.print("Enter the index ");
          index = scan.nextInt();
          newArray1 = delete(num,index);
          String out1="The output array is ";
          out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
          System.out.println(out1);

            System.out.print("Enter the target value ");
          target = scan.nextInt();
          newArray2 = remove(num,target);
          String out2="The output array is ";
          out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
          System.out.println(out2);

          System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
          answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));
        }

        public static String listArray(int num[]){
        String out="{";
        for(int j=0;j<num.length;j++){
          if(j>0){
            out+=", ";
          }
          out+=num[j];
        }
        out+="} ";
        return out;
        }
}
