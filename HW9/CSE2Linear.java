/////// Kyra Dimaranan
/////// CSE 002
/////// This program prompts user to enter students' grades and verifies whether they are entered correctly

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{
  
 public static void print(int[] input){
   for (int p=0; p < input.length; p++){
     System.out.print(input[p] + " ");   
   }
   System.out.println("");
 }
  
 public static int[] scramble(int[] input){
   Random randomGen = new Random();  
   int d = randomGen.nextInt(input.length);
   for (int b=0; b < input.length; b++){
     int first = input[b];
     input[b] = input[d]; 
     input[d] = first;
   }
   return input; 
 }
  
  public static int binarySearch(int[] input, int scoreSearch){ 
    int found = 0;
    int mid = input.length/2;
    if (input[mid] == scoreSearch){
      found = 1;
    }
    else if(input[mid] < scoreSearch){
      for (int i=mid; i < input.length; i++){
        if(input[i] == scoreSearch){
          found = 1;
        }
        else{
          found=0;
        }
      }
    }
    else{
      for(int i=0; i < mid; i++){
        if(input[i] == scoreSearch){
          found=1;
        }
        else{
          found=0;
        }
      }
    }
    return found;

  }
  
  public static int linearSearch(int[] input, int scoreSearch){
    int found=0;
    for (int v=0; v < input.length; v++){
      if (input[v] == scoreSearch){
        found=1;
      }
      else {
        found=0;;
      }
    }
    return found;
  }
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //declare new scanner
    int c=0; //increment for while loop
    boolean[] checkgrade = new boolean[15]; //boolean condition for check if input is an integer
    //boolean correct;
    int[] grades = new int[checkgrade.length];
     
  System.out.println("Enter 15 ascending ints for final grades in CSE2: "); // prompts user to enter grades  
    for (int i=0; i < 15; i++){ //checks the following conditions for each element of the array
      grades[i] = myScanner.nextInt();
      checkgrade[i] = myScanner.hasNextInt(); 
        while(c==0){ 
          if(checkgrade[i] == true){
            c++;
          }
          else{
            myScanner.next();
            System.out.println("Please only input integers");
          }
        }
  }
    for (int j=0; j < grades.length; ++j){
       if (grades[j] < 0 || grades[j] > 100){ //checks if scores are within range
          System.out.println("Score not in range");
          myScanner.next();
            } 
    }

   print(grades); //prints array 'grades'
   System.out.println("Enter a grade to be searched for: ");
   int scoreSearch = myScanner.nextInt();
   int found = binarySearch(grades, scoreSearch); //performs binary search on orig array
    if (found == 0){
      System.out.println("Score was not found");
    }
    else{
      System.out.println("Score was found");
    }
   int[] scramGrades = scramble(grades); //makes new array for the scrambled original array
   print(scramGrades); //prints out scrambled array
   System.out.println("Enter a grade to be searched for: ");
   int scoreSearch2 = myScanner.nextInt();
   int found2 = linearSearch(scramGrades, scoreSearch2); //performs linear search on scrambled array
    if (found2 == 0){
      System.out.println("Score was not found");
    }
    else{
      System.out.println("Score was found");
    }
  } 
}