/////// Kyra Dimaranan
////// CSE 002
////// This program will use various methods to generate different kinds of words and make a sentence
import java.util.Random;
import java.util.Scanner;

public class lab07{
  
  public static String adjective(){
    String randAdjec= " "; 
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt) {
      case 0:
        randAdjec = "lazy";
        break;
      case 1:
        randAdjec = "brown";
        break;
      case 2:
        randAdjec = "quick";
        break;
      case 3:
        randAdjec = "scary";
        break;
      case 4:
        randAdjec = "slow";
        break;
      case 5:
        randAdjec = "black";
        break;
      case 6:
        randAdjec = "spotted";
        break;
      case 7:
        randAdjec = "dumb";
        break;
      case 8:
        randAdjec = "sleepy";
        break;
      case 9:
        randAdjec = "white";
        break;
    }
    System.out.println(randAdjec);
  return randAdjec;
  }
  
  public static String subject(){
    String randSub = "";
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        randSub = "fox";
        break;
      case 1:
        randSub = "cat";
        break;
      case 2:
        randSub = "dog";
        break;
      case 3:
        randSub = "bird";
        break;
      case 4:
        randSub = "mouse";
        break;
      case 5:
        randSub = "moose";
        break;
      case 6:
        randSub = "monkey";
        break;
      case 7:
        randSub = "duck";
        break;
      case 8:
        randSub = "pig";
        break;
      case 9:
        randSub = "fish";
        break;
    }
    System.out.println(randSub);
    return randSub;
  }
  
  public static String verb(){
    String pasVerb = " ";
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        pasVerb = "chased";
        break;
      case 1:
        pasVerb = "attacked";
        break;
      case 2:
        pasVerb = "fought";
        break;
      case 3:
        pasVerb = "slept";
        break;
      case 4:
        pasVerb = "napped";
        break;
      case 5:
        pasVerb = "ran";
        break;
      case 6:
        pasVerb = "walked";
        break;
      case 7:
        pasVerb = "tripped";
        break;
      case 8:
        pasVerb = "swam";
        break;
      case 9:
        pasVerb = "jumped";
        break;
    }
    System.out.println(pasVerb);
    return pasVerb;
  }
  
  public static String object(){
    String randOb = "";
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10);
    switch (randomInt){
      case 0:
        randOb = "plane";
        break;
      case 1:
        randOb = "car";
        break;
      case 2:
        randOb = "tricycle";
        break;
      case 3:
        randOb = "bike";
        break;
      case 4:
        randOb = "tank";
        break;
      case 5:
        randOb = "bed";
        break;
      case 6:
        randOb = "house";
        break;
      case 7:
        randOb = "mailbox";
        break;
      case 8:
        randOb = "water";
        break;
      case 9:
        randOb = "door";
        break;
    }
    System.out.println(randOb);
    return randOb;
  }
  
  public static void main (String[] args){
    int c=0; //initalizes increment for loop
    String sub= "";
    String wordv="";
    String randAdjec="";
    String randOb="";
    int sent = 0;
    Scanner myScanner = new Scanner(System.in);
    while (c==0){
      System.out.println("The");
      randAdjec = adjective();
      sub = subject();
      wordv = verb();
      System.out.println("the");
      randOb = object();
      System.out.println("Type 1 if you want another sentence: ");
      sent = myScanner.nextInt();
      if (sent != 1){
        c++;
      }
      
    }
  }
}